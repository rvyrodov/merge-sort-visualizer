import React from 'react';
import styles from './Button.module.css';

export default class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  onclick = event => {
    event.preventDefault()
    this.props.onclick()
  }

  render() {
    return<button className={styles.button} onClick={this.onclick} title={this.props.title}>
      <img height="40" width="40" src={this.props.img} alt=""/>
    </button>
  }
}
