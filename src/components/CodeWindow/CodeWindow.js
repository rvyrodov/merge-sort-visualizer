import React from "react";
import styles from "./CodeWindow.module.css"
import CodeString from "../CodeString/CodeString";

export default function CodeWindow(props) {
    let k = 0;
    return <div className={styles.codeWindow}>
        {props.codeStrings.map(string => {
            return <CodeString
              text={string}
              green={props.codeGreen[k++]}
            />
          }
        )}
    </div>
}