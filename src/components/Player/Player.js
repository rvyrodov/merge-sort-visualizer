import React from "react";
import styles from "./Player.module.css"
import Button from "../Button/Button";

export default class Player extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  start = () => {
    this.props.start()
  }

  prev = () => {
    this.props.prev()
  }

  pause = () => {
    this.props.pause()
  }

  play = () => {
    this.props.play()
  }

  next = () => {
    this.props.next()
  }

  finish = () => {
    this.props.finish()
  }

  render() {
    return <div className={styles.player}>
      <Button title="В начало" onclick={this.start} img="start.svg"/>
      <Button title="Назад" onclick={this.prev} img="prev.svg"/>
      <Button title="Пауза" onclick={this.pause} img="pause.svg"/>
      <Button title="Запустить" onclick={this.play} img="play.svg"/>
      <Button title="Вперед" onclick={this.next} img="next.svg"/>
      <Button title="В конец" onclick={this.finish} img="finish.svg"/>
    </div>
  }
}