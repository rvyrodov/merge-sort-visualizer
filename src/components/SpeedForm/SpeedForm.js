import React from 'react';
import styles from './SpeedForm.module.css';

export default class ArrayForm extends React.Component {

  handleSubmitSlow = event => {
    event.preventDefault();
    this.props.handleSubmit(2000);
  };

  handleSubmitMedium = event => {
    event.preventDefault();
    this.props.handleSubmit(1000);
  };

  handleSubmitFast = event => {
    event.preventDefault();
    this.props.handleSubmit(500);
  };

  render() {
    return (
      <div className={styles.form}>
        <button className={styles.button} onClick={this.handleSubmitSlow}>{"Медленно"}</button>
        <button className={styles.button} onClick={this.handleSubmitMedium}>{"Средне"}</button>
        <button className={styles.button} onClick={this.handleSubmitFast}>{"Быстро"}</button>
      </div>
    );
  }
}
