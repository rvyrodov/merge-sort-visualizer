import React from "react";
import styles from "./Array.module.css"
import Element from "../Element/Element";

export default function Array(props) {
  let k = 0;
  return <div className={styles.array}>
    {props.numbers.map(number => {
      k++;
      return <Element
        number={number}
        noview={props.hasview && props.noview[k - 1]}
        under={props.hasunder && props.under[k - 1]}
        block={props.block[k - 1]}
        siz={props.size[k - 1]}
      />
    })}
  </div>
}