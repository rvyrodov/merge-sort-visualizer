import React from "react";
import styles from "./CodeString.module.css"

export default function CodeString(props) {
    let style = styles.codeString
    if (props.green) {
        style += " " + styles.green
    }
    return <div className={style}>{props.text}</div>
}