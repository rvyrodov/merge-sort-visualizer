import React from "react";
import styles from "./Emelemt.module.css"

const colors = [
  "black",
  "gray",
  "darkred",
  "red",
  "tomato",
  "lightsalmon",
  "darkorange",
  "orange",
  "gold",
  "goldenrod",
  "yellow",
  "olive",
  "greenyellow",
  "darkgreen",
  "green",
  "limegreen",
  "lime",
  "cyan",
  "deepskyblue",
  "dodgerblue",
  "steelblue",
  "blue",
  "darkblue",
  "midnightblue",
  "blueviolet",
  "indigo",
  "darkviolet",
  "purple",
  "magenta",
  "hotpink",
]

export default function Element(props) {
  let no = ""
  let style = {}
  if (props.noview) {
    no += " " + styles.noview
    style = {
      display: "none",
      height: 100 * props.siz,
      backgroundColor: colors[props.block - 1],
    }
  } else {
    style = {
      height: 100 * props.siz,
      backgroundColor: colors[props.block - 1],
    }
  }
  return <div className={styles.element + no}>
    <div className={styles.block} style={style}></div>
    <div className={styles.number + no}>{props.number}</div>
    <div className={styles.number + no}>{props.under}</div>
  </div>
}