import React from 'react';
import styles from './ArrayForm.module.css';

const genArray = "Случайный массив"
const addArray = "Добавить массив"

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export default class ArrayForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputString:"",
      buttonText: genArray,
    };
  }

  clearForm() {
    this.setState({
      inputString:"",
      buttonText: genArray,
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    let result = []
    if (this.state.inputString !== "") {
      let current = ""
      for (let i = 0; i < this.state.inputString.length; ++i) {
        if (/[0-9]/.test(this.state.inputString[i])) {
          current += this.state.inputString[i]
        } else {
          result.push(+current)
          current = ""
        }
      }
      if (current !== "") {
        result.push(+current)
        current = ""
      }
    } else {
      let n = getRandomInt(21) + 9;
      for (let i = 0; i < n; ++i) {
        result.push(getRandomInt(31));
      }
    }
    this.props.handleSubmit(result);
    this.clearForm()
  };

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    }, () => {
      if (this.state.inputString === "") {
        this.setState({
          inputString: this.state.inputString,
          buttonText: genArray,
        })
      } else {
        this.setState({
          inputString: this.state.inputString,
          buttonText: addArray,
        })
      }
    });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} className={styles.form}>
          <input
            name="inputString"
            placeholder="Введите числа через пробел"
            value={this.state.inputString}
            onChange={this.handleInputChange}
            className={styles.input}
          />
          <button type="submit" className={styles.button}>{this.state.buttonText}</button>
      </form>
    );
  }
}
