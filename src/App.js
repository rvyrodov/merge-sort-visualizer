import React, {Fragment} from 'react';
import './App.css';
import Array from "./components/Array/Array";
import CodeWindow from "./components/CodeWindow/CodeWindow";
import Player from "./components/Player/Player";
import ArrayForm from "./components/ArrayForm/ArrayForm";
import SpeedForm from "./components/SpeedForm/SpeedForm";

const array = [10, 3, 1, 8, 2, 3, 5, 7, 6, 4];
const noview = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const size = [1, 0.3, 0.1, 0.8, 0.2, 0.3, 0.5, 0.7, 0.6, 0.4];
const under = ["", "", "", "", "", "", "", "", "", "", "", ""];
const block = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
const save = [];
let intervalID = undefined;
const codeGreen = [1, 0, 0, 0, 0, 0, 0];
const codeText = [
  "разделим массив на блоки размера 1",
  "итеративно соединим соседние блоки:",
  "    for i = leftBegin to rightEnd",
  "        if leftValue <= rightValue",
  "            copy leftValue",
  "        else: copy rightValue",
  "вернем элементы обратно в массив",
];
const moments = [];

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      array,
      noview,
      save,
      codeGreen,
      size,
      block,
      under,
      moment: 0,
      speed: 1000,
    }
    this.generateSort();
  }

  generateSort() {
    moments.splice(0, moments.length)
    let sortarray   = this.state.array.slice().sort((a, b) => (a - b))
    let n = this.state.array.length
    let carray      = this.state.array.slice()
    let cnoview     = this.state.noview.slice()
    let csave       = this.state.save.slice()
    let ccodeGreen  = this.state.codeGreen.slice()
    let cunder      = this.state.under.slice()
    let csize = []
    let csavesize = []
    let cblock = []
    let csaveblock = []
    for (let i = 0; i < n; ++i) {
      csize.push((sortarray.indexOf(carray[i]) + 1) / n)
      cblock.push(i + 1)
    }
    let mom = {
      array:      carray.slice(),
      under:      cunder.slice(),
      noview:     cnoview.slice(),
      save:       [],
      codeGreen:  ccodeGreen.slice(),
      size:       csize.slice(),
      savesize:   [],
      block:      cblock.slice(),
      saveblock:  [],
    }
    this.setState(mom)
    moments.push(mom)
    for (let i = 1; i < n; i *= 2) {
      let cb = 1;
      for (let j = 0; j < n - i; j += 2 * i) {
        console.log(cb)
        let left = j;
        let mid = j + i;
        let right = Math.min(n, j + 2 * i);
        cunder[left] = "left"
        cunder[mid] = "right"
        ccodeGreen[0] = ccodeGreen[1] = ccodeGreen[4] = ccodeGreen[6] = ccodeGreen[3] = ccodeGreen[5] = 0
        ccodeGreen[2] = 1
        let li = 0;
        let ri = 0;
        let cmoment = {
          array:      carray.slice(),
          under:      cunder.slice(),
          noview:     cnoview.slice(),
          save:       csave.slice(),
          size:       csize.slice(),
          savesize:   csavesize.slice(),
          codeGreen:  ccodeGreen.slice(),
          block:      cblock.slice(),
          saveblock: csaveblock.slice(),
        }
        moments.push(cmoment)
        while (left + li < mid && mid + ri < right) {
          if (carray[left + li] <= carray[mid + ri]) {
            csave.push(carray[left + li]);
            csavesize.push(csize[left + li])
            csaveblock.push(cb)
            cunder[left + li] = ""
            if (left + li + 1 < mid) {
              cunder[left + li + 1] = "left"
            }
            console.log(csaveblock)
            cnoview[left + li] = 1;
            ccodeGreen[0] = ccodeGreen[1] = ccodeGreen[5] = ccodeGreen[6] = 0
            ccodeGreen[2] = ccodeGreen[3] = ccodeGreen[4] = 1
            li++;
          } else {
            csave.push(carray[mid + ri])
            csavesize.push(csize[mid + ri])
            csaveblock.push(cb)
            cunder[mid + ri] = ""
            if (mid + ri + 1 < right) {
              cunder[mid + ri + 1] = "right"
            }
            console.log(csaveblock)
            cnoview[mid + ri] = 1;
            ccodeGreen[0] = ccodeGreen[1] = ccodeGreen[4] = ccodeGreen[6] = 0
            ccodeGreen[2] = ccodeGreen[3] = ccodeGreen[5] = 1
            ri++;
          }
          let cmoment = {
            array:      carray.slice(),
            under:      cunder.slice(),
            noview:     cnoview.slice(),
            save:       csave.slice(),
            size:       csize.slice(),
            savesize:   csavesize.slice(),
            codeGreen:  ccodeGreen.slice(),
            block:      cblock.slice(),
            saveblock: csaveblock.slice(),
          }
          moments.push(cmoment)
        }
        while (left + li < mid) {
          csave.push(carray[left + li]);
          csavesize.push(csize[left + li])
          cunder[left + li] = ""
          if (left + li + 1 < mid) {
            cunder[left + li + 1] = "left"
          }
          csaveblock.push(cb)
          console.log(csaveblock)
          cnoview[left + li] = 1;
          ccodeGreen[0] = ccodeGreen[1] = ccodeGreen[5] = ccodeGreen[6] = 0
          ccodeGreen[2] = ccodeGreen[3] = ccodeGreen[4] = 1
          li++;
          let cmoment = {
            array:      carray.slice(),
            under:      cunder.slice(),
            noview:     cnoview.slice(),
            save:       csave.slice(),
            size:       csize.slice(),
            savesize:   csavesize.slice(),
            codeGreen:  ccodeGreen.slice(),
            block:      cblock.slice(),
            saveblock:  csaveblock.slice(),
          }
          moments.push(cmoment)
        }
        while (mid + ri < right) {
          csave.push(carray[mid + ri])
          cnoview[mid + ri] = 1;
          cunder[mid + ri] = ""
          if (mid + ri + 1 < right) {
            cunder[mid + ri + 1] = "right"
          }
          csavesize.push(csize[mid + ri])
          csaveblock.push(cb)
          console.log(csaveblock)
          ccodeGreen[0] = ccodeGreen[1] = ccodeGreen[4] = ccodeGreen[6] = 0
          ccodeGreen[2] = ccodeGreen[3] = ccodeGreen[5] = 1
          ri++;
          let cmoment = {
            array:      carray.slice(),
            under:      cunder.slice(),
            noview:     cnoview.slice(),
            save:       csave.slice(),
            size:       csize.slice(),
            savesize:   csavesize.slice(),
            codeGreen:  ccodeGreen.slice(),
            block:      cblock.slice(),
            saveblock: csaveblock.slice(),
          }
          moments.push(cmoment)
        }
        for (let q = 0; q < li + ri; ++q) {
          carray[left + q] = csave[0]
          cnoview[left + q] = 0
          cblock[left + q] = csaveblock[0]
          csize[left + q] = csavesize[0]
          ccodeGreen[0] = ccodeGreen[1] = ccodeGreen[4] = ccodeGreen[2] = ccodeGreen[3] = ccodeGreen[5] = 0
          ccodeGreen[6] = 1
          csave.shift()
          csavesize.shift()
          csaveblock.shift()
          if (csave.length === 0) {
            ccodeGreen[6] = 0
          }
          let cmoment = {
            array:      carray.slice(),
            under:      cunder.slice(),
            noview:     cnoview.slice(),
            save:       csave.slice(),
            size:       csize.slice(),
            savesize:   csavesize.slice(),
            codeGreen:  ccodeGreen.slice(),
            block:      cblock.slice(),
            saveblock:  csaveblock.slice(),
          }
          moments.push(cmoment)
        }
        cb++
        console.log(cb)
      }
    }
  }

  handleNext = () => {
    this.handlePause()
    let cm = this.state.moment
    if (cm + 1 < moments.length) {
      this.setState({
        array:      moments[cm + 1].array,
        under:      moments[cm + 1].under,
        noview:     moments[cm + 1].noview,
        save:       moments[cm + 1].save,
        size:       moments[cm + 1].size,
        savesize:   moments[cm + 1].savesize,
        codeGreen:  moments[cm + 1].codeGreen,
        block:      moments[cm + 1].block,
        saveblock:  moments[cm + 1].saveblock,
        moment:     cm + 1,
        speed:      this.state.speed,
      })
    }
  }
  handlePrev = () => {
    this.handlePause()
    let cm = this.state.moment
    if (cm > 0) {
      this.setState({
        array: moments[cm - 1].array,
        under:      moments[cm - 1].under,
        noview: moments[cm - 1].noview,
        save: moments[cm - 1].save,
        size:       moments[cm - 1].size,
        savesize:   moments[cm - 1].savesize,
        codeGreen: moments[cm - 1].codeGreen,
        block:      moments[cm - 1].block,
        saveblock:  moments[cm - 1].saveblock,
        moment: cm - 1,
        speed:      this.state.speed,
      })
    }
  }
  handlePause = () => {
    if (intervalID) {
      clearInterval(intervalID)
      intervalID = undefined
    }
  }
  handlePlay = () => {
    if (this.state.moment + 1 < moments.length) {
      intervalID = setInterval(() => {
        let cm = this.state.moment
        if (cm + 1 < moments.length) {
          this.setState({
            array: moments[cm + 1].array,
            under:      moments[cm + 1].under,
            noview: moments[cm + 1].noview,
            save: moments[cm + 1].save,
            size:       moments[cm + 1].size,
            savesize:   moments[cm + 1].savesize,
            codeGreen: moments[cm + 1].codeGreen,
            block:      moments[cm + 1].block,
            saveblock:  moments[cm + 1].saveblock,
            moment: cm + 1,
            speed:      this.state.speed,
          })
        }
      }, this.state.speed)
    } else {
      this.handlePause()
    }
  }
  handleStart = () => {
    this.handlePause()
    this.setState({
      array: moments[0].array,
      under:      moments[0].under,
      noview: moments[0].noview,
      save: moments[0].save,
      size:       moments[0].size,
      savesize:   moments[0].savesize,
      codeGreen: moments[0].codeGreen,
      block:      moments[0].block,
      saveblock:  moments[0].saveblock,
      moment: 0,
      speed:      this.state.speed,
    })
  }
  handleFinish = () => {
    this.handlePause()
    let p = moments.length - 1
    this.setState({
      array: moments[p].array,
      under:      moments[p].under,
      noview: moments[p].noview,
      save: moments[p].save,
      size:       moments[p].size,
      savesize:   moments[p].savesize,
      codeGreen: moments[p].codeGreen,
      block:      moments[p].block,
      saveblock:  moments[p].saveblock,
      moment: p,
      speed:      this.state.speed,
    })
  }

  handleSubmitArray = (numbers) => {
    this.handlePause()
    let nw = []
    let cs = [0, 0, 0, 0, 0, 0, 0]
    let under = ["", ""]
    for (let i = 0; i < numbers.length; ++i) {
      if (i > 1) {
        under.push("")
      }
      nw.push(0)
    }
    this.setState({
      array: numbers,
      under: under,
      noview: nw,
      codeGreen: cs,
      save: [],
      size:       [],
      savesize:   [],
      block:      [],
      saveblock:  [],
      moment: 0,
      speed:      this.state.speed,
    }, () => {this.generateSort()})
  }

  handleSubmitSpeed = (number) => {
    this.handlePause()
    this.setState({
      array:      this.state.array,
      under:      this.state.under,
      noview:     this.state.noview,
      codeGreen:  this.state.codeGreen,
      save:       this.state.save,
      size:       this.state.size,
      savesize:   this.state.savesize,
      block:      this.state.block,
      saveblock:  this.state.saveblock,
      moment:     this.state.moment,
      speed:      number,
    })
  }

  render() {
    return <Fragment>
      <Array
        numbers={this.state.array}
        noview={this.state.noview}
        size={this.state.size}
        block={this.state.block}
        hasview={true}
        hasunder={true}
        under={this.state.under}
      />
      <Array
        numbers={this.state.save}
        size={this.state.savesize}
        block={this.state.saveblock}
        hasview={false}
        hasunder={false}
      />
      <CodeWindow
        codeStrings={codeText}
        codeGreen={this.state.codeGreen}
      />
      <Player
        start={this.handleStart}
        prev={this.handlePrev}
        pause={this.handlePause}
        play={this.handlePlay}
        next={this.handleNext}
        finish={this.handleFinish}
      />
      <SpeedForm handleSubmit={this.handleSubmitSpeed}/>
      <ArrayForm handleSubmit={this.handleSubmitArray}/>
    </Fragment>
  }
}

export default App;
